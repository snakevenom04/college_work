# Take home exam 
## Due Date : 3rd March 2020, 11 p.m IST
## Instructions
* Answers in 10-15 lines each

## Questions
1. In the fundamentals of fMRI, what are the changes or effects of oxygenation or de-oxygenation on dephasing? what is the basic difference between the BOLD signal and hemodynamic response  system?

1. In the neuroscience of learning - the importance of phonological skills was stressed - as per the research presented by the scientist how is this explained (in terms of its criticality). Second, how does this work in multilingual children - as per your own understanding?

1. In the neurobiology of emotion video - the James Lange theory fails to explain a few conditions, can you list them (one is explained by the instructor, give an example from your own understanding)?

1. What is associative learning and how is being connected to emotion? Name the parts of the brain linked to the concept of emotion and associative learning.

## Official Resources:

* [Fundamentals of FMRI](https://www.coursera.org/lecture/neuroscience-neuroimaging/basics-of-fmri-mC6Om)

* [Neuroscience of learning](https://www.youtube.com/watch?v=5_6fezBz9IA)

* [Neuroscience of emotions Part I](https://pt.coursera.org/lecture/medical-neuroscience/neurobiology-of-emotion-part-1-AcqZW)

* [Neuroscience of emotions Part II](
https://www.coursera.org/lecture/medical-neuroscience/neurobiology-of-emotion-part-2-GklPh)


